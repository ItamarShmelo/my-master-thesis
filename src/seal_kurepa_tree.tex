\section{The Sealing Poset}

In this section we provide the first part of the main proof of this work. Here we present the forcing-poset $\mathbb{S}$ (called the sealing poset) and prove that $\mathbb{S}$ is proper for $\mathcal{S}\cup \mathcal{T}$. From this property we conclude that it preserves $\omega_1$ and $\omega_2$. The proof for properness relies heavily on reflection between a model and its elementary submodel, to demonstrate this method and its strength we use it on a simpler case. We start by providing a proof of a theorem by Baumgartner.

\begin{definition}
    Let $T$ be an Aronszajn tree, we define the specializing forcing notion
    \begin{equation*}
        \mathbb{A} = \left\{ f:T\rightarrow \omega\ \Big|\ |f|<\aleph_0 \wedge \forall x,y\in \mathrm{dom}\: f, x \parallel y\rightarrow x = y\vee f(x)\neq f(y) \right\}
    \end{equation*}
\end{definition}

\begin{theorem}(Baumgartner \cite{JechBaumgartner})
    Let $T$ be an Aronszajn tree then $\mathbb{A}$ adds a specializing function of $T$ without ruining the Aronszajn property.
\end{theorem}

\begin{remark}
    This is to be considered as a warm-up proof before the main proof later in this section. We did not find a place in the literature where this proof is presented. Still, we do not presume that this is an original result.
\end{remark}

\begin{proof}
    In his proof, Baumgartner showed that $\mathbb{A}$ is c.c.c. which implies that $\omega_1$ is preserved. To achieve the same we prove that $\mathbb{A}$ is proper for every countable elementary submodel (which also implies $\omega_1$ is preserved).

    The cardinality of $\mathbb{A}$ is $\aleph_1$ let $M\prec H((2^{\aleph_1})^ +)$ be a countable elementary submodel. Assume $\mathbb{A}$ is not proper for $M$ then $\exists  p\in \mathbb{A}$ s.t. $\forall q\leq p$, $q$ is not $M$-generic. Thus, $p$ itself is not $M$-generic then $\exists D\in M, D\subset\mathbb{A}$ dense and $\exists p' \leq p$  s.t. $\forall r\in D\cap M,\, r\perp p'$. Since $D$ is dense $\exists d\in D$ s.t. $d\leq p'$ thus $\forall r\in D\cap M$ we have $r\perp d$. Consider the set

    \begin{equation*}
        \mathfrak{D} = \left\{ r\in D\, \Big|\, |d| = |r| \wedge r\leq d^M\right\}
    \end{equation*}

    Where $d^M = d\cap M$. Since $d^M\in M$ (since $d^M$ is finite), $\mathfrak{D}$ is definable in $M$ therefore $\mathfrak{D}\in M$. Enumerate the domains of the conditions in $\mathfrak{D}$ s.t. the enumeration of $d^M$ is constant across all conditions in $\mathfrak{D}$. Then the reason for the incompatibility of a condition $r\in D\cap M$ and $d$ is $\exists i,j <|d|$ s.t. $r_j\leq d_i$ and $r(r_j) = d(d_i)$.

    We show that there exists a coloring $P_{ij}$ of $\mathfrak{D}$ for $i,j < |d|$ with the following property

    \begin{equation*}
        (*)\quad r^1, r^2\in P_{ij} \rightarrow r^1_j \parallel r^2_j \wedge r^1(r^1_j) = r^2(r^2_j) = d(d_i)
    \end{equation*}

    Let $X\subset\mathfrak{D}\cap M$ be a \textbf{finite} set and consider the set

    \begin{equation*}
        P_{ij}^X = \left\{ r\in X\, \Big|\, r_j\leq d_i \wedge r(r_j) = d(d_i)\right\}
    \end{equation*}

    Since every element in $X$ is incompatible with $d$ it has to be in one of the $P_{ij}^X$ hence $X = \bigcup_{ij < |d|} P_{ij}^X$.

    The sets $P_{ij}^X$ are definable in $H(\omega_3)$ but not in $M$, to get the coloring of $X$ inside $M$ we use the following trick

    \begin{equation*}
        H(\omega_3) \models \exists P_{ij}^X,\, \left(X = \bigcup_{ij < |d|} P_{ij}^X \right) \wedge \left(P_{ij}^X\text{ has property } (*)\right)
    \end{equation*}

    Since all parameters used in $(*)$ are internal to $M$ (notice that $d$ is not used but $d(d_j)\in \omega$ which is in $M$) we get from elementarity that

    \begin{equation*}
        M \models \exists P_{ij}^X,\, \left(X = \bigcup_{ij < |d|} P_{ij}^X \right) \wedge \left(P_{ij}^X\text{ has property } (*)\right)
    \end{equation*}

    Let $P_{ij}^{M,X}$ be the coloring of $X$ inside $M$. To get the coloring of $\mathfrak{D}$ we use a compactness argument. Add $|\mathfrak{D}|$ constants $c_\alpha$ to the language, enumerate $\mathfrak{D}$ inside $M$, add the constants $\widetilde{P}_{ij}$ and consider the theory

    \begin{equation*}
        \mathfrak{T}_1 = \left\{c_\alpha\notin \widetilde{P}_{ij} \vee c_\beta\notin \widetilde{P}_{ij}\,  \Big|\, \alpha,\beta < |\mathfrak{D}| \wedge i,j < |d|, r^\alpha_j \perp r^\beta_j\right\}
    \end{equation*}

    \begin{equation*}
        \mathfrak{T}_2 = \left\{c_\alpha\notin \widetilde{P}_{ij}\,  \Big|\, \alpha < |\mathfrak{D}| \wedge i,j < |d|\wedge r^\alpha(r^\alpha_j) \neq d(d_i)\right\}
    \end{equation*}

    \begin{equation*}
        \mathfrak{T}_3 = \left\{\bigvee_{ij <|d|} c_\alpha \in \widetilde{P}_{ij}\, \Big|\, \alpha<|\mathfrak{D}| \right\}
    \end{equation*}

    Define $\mathfrak{T} = \mathfrak{T}_1\cup \mathfrak{T}_2\cup\mathfrak{T}_3$. We show that

    \begin{equation*}
        M\models \mathfrak{T} \text{ is finitely satisfiable}
    \end{equation*}

    Let $\mathcal{X}\subset \mathfrak{T}$ be
    some finite set of formulas and let

    \begin{equation*}
        X = \left\{ \alpha\, \bigg|\,  c_\alpha \text{ is in a formula of }\mathcal{X} \right\}
    \end{equation*}

    Interpret $c_\alpha$ as $r^\alpha$ and $\widetilde{P}_{ij}$ as $P_{ij}^{X,M}$. By the previous discussion this then satisfies $\mathcal{X}$, since this is true for every $\mathcal{X}$ finite, we get that $\mathfrak{T}$ is finitely satisfiable. Thus, by compactness there exists $P_{ij}$ that satisfy all of $\mathfrak{T}$. Thus

    \begin{equation*}
        M\models P_{ij} \text{ is a coloring of } \mathfrak{D}\text{ with property } (*)
    \end{equation*}

    Hence, by elementarity

    \begin{equation*}
        H(\omega_3)\models P_{ij} \text{ is a coloring of } \mathfrak{D}\text{ with property } (*)
    \end{equation*}

    Hence, $\exists i,j < |d|$ s.t. $d\in P_{ij}$. We show that $d\in P_{ij}$ either leads to a contradiction or to the existence of a subset $X$ of $P_{ij}$ s.t. $d\in X$ and all elements of $X$ there $j$'th element is compatible with the $i$'th of $d$. First consider the case when $d_j\notin M$, in this case, we have that  $P_{ij}$ is unbounded and then since $\forall r^1,r^2\in P_{ij},\, r^1_j\parallel r^2_j$, $P_{ij}$ defines the branch

    \begin{equation*}
        B = \bigcup\left\{ seg(r_j)\, \Big|\, r\in P_{ij} \right\}
    \end{equation*}

    This is a branch of $T$ in $M$, but from elementary $M$ also thinks that $T$ is Aronszajn, contradiction. We remain with the case $d_j\in M$, consider the set

    \begin{equation*}
        X = \left\{ r\in P_{ij}\, \Big|\, r_j = d_j \wedge r(r_j) = d(d_j)\right\}
    \end{equation*}

    Let $r\in X$ be some condition, then since $r_j = d_j\wedge r(r_j) = d(d_j)$ we have that $r_j$ can't interfere with $d_i$. After a finite number of times applying this argument we would get a set (in $M$) with $d$ in it s.t. all the conditions in it are compatible to $d$. And since $M$ would think that this set is not empty we get a condition in $M$ compatible to $d$, contradicting our assumption.
\end{proof}

We now begin the main proof of this section.

\begin{definition}
    \label{def:forcing_poset}
    Let $T$ be a Kurepa tree, define the \underline{sealing poset of $T$}, $\mathbb{S}$: \hfill

    $p\in\mathbb{S}$ iff $p =\left< f_p, \vec{M}_p\right>$ where $f_p : T\cup \left[T\right]\rightarrow\omega$ and $\vec{M}_p \in \mathbb{M}$ is a finite sequence of elementary submodels of two types. We also require that our conditions have the following properties:
    \begin{enumerate}
        \item Monotonic: $\forall x\leq_T y\in \mathrm{dom}f_p,\ f_p\left( x \right)\leq f_p \left( y \right)\wedge \forall x,b\in \mathrm{dom}f_p,b\in \left[T\right]\wedge x\in b\rightarrow f_p\left( x \right)\leq f_p\left( b \right)$.
        \item \label{condition:2}$\forall N\in \vec{M}_p\forall b\in N\left( \exists t\in \left(\mathrm{dom}f_p\backslash N\right) \cap b \right) \rightarrow \left( b\in\mathrm{dom}f_p\wedge f_p\left( b \right) = f_p\left( t \right) \right)$.
        \item $\forall x_1, x_2 \in \mathrm{dom}f_p,\ x_1 \wedge x_2\in \mathrm{dom}f_p$.
        \item $\emptyset_{root}\in \mathrm{dom}f_p\wedge f_p \left( \emptyset_{root} \right) = 0$.
    \end{enumerate}

    $\mathbb{S}$ is ordered by reverse inclusion.
\end{definition}

\begin{remark}
    We want our forcing to add a generic function $f:T\cup[T]\rightarrow\omega$ without collapsing $\omega_1$ or $\omega_2$. Now in $M[G]$ if a new generic branch $b$ is added via some other forcing we want $f\restriction b$  to be the cause of $\omega_1$ collapse.

    Since we require monotonicity of $f$ we expect that $f\restriction b$ would then be unbounded.

    Condition \ref{condition:2} is needed to ensure properness. We need $N$ to know the bound on the value of a branch $b\in N$ that has a bound (from monotonicity) outside of $N$.

    Condition 3 is needed in case there are two branches which are determined by condition \ref{condition:2} have a common ancestor which determines them both. In this case we want to make sure both branches are compatible.

    Condition 4 sets the root to 0, which removes a redundant degree of freedom for our function.
\end{remark}

\begin{theorem}
    \label{thm:P_is_proper}
    $\mathbb{S}$ is proper for $\mathcal{S}$. In particular $\mathbb{S}$ does not collapse $\omega_1$.
\end{theorem}

\begin{remark}
    To prove $\mathbb{S}$ is proper we need to show that for a club many $M\in \mathcal{S}$ and $\forall p\in \mathbb{S}\cap M$ there exists an extension $q\leq p$ which is an $M$-generic condition.
\end{remark}

\begin{proof}[Proof of Theorem \ref{thm:P_is_proper}]
    \renewcommand{\qedsymbol}{}
    Let $\lambda = \left(2^{|\mathbb{S}|}\right)^{+}$. Toward a contradiction, let's assume that $M\in \mathcal{S}$ s.t. $\exists M'\prec H(\lambda)\wedge M'\cap H(\omega_2) = M$, and that $\exists p \in M$ s.t. $\forall q \leq p$, $q$ is not $M$-generic. In particular $p$ is not $M$-generic, thus $\exists D_p\in M$ dense s.t. $\forall r \in M\cap D_p, r\perp q$. Since $D_p$ is dense $\exists d\in D_p$ s.t. $d\leq q$, this $d$ will also satisfy $\forall r\in M\cap D_p,\ r\perp d$. It follows that $d\notin M$ (else we take $r = d$ and get a contradiction).

    Denote $d^M =\left< f_d\restriction M,\ \vec{M}_d\cap M\right>$.

    Since $p\in M$, the sequence of models is also in $M$. From corollary \ref{cor:p_cup_M} we have a sequence of models $\vec{M}_r\leq \vec{M}_p$ which is the closure of $\vec{M}_p\cup \left\{ M \right\}$ under intersections. Consider $\tilde{r} = \left< f_p,\vec{M}_r\right>\leq p$ if we manage to prove $\tilde{r}$ is a condition then it will also have no extension to an $M$-generic condition. Thus taking $D\equiv D_{\tilde{r}}$ and $d\in D$ we get that $M\in \vec{M}_d$.
\end{proof}

\begin{claim}
    $\tilde{r}$ is a condition.
\end{claim}

\begin{proof}


    In order to show that $\tilde{r}\in \mathbb{S}$ all we need to show is that condition (\ref{condition:2}) from definition \ref{def:forcing_poset} is satisfied (since all the changes are done to is the model sequence). Let $N\in \vec{M}_{\tilde{r}}$, by \ref{cor:p_cup_M}, $N$ is the intersection of models $N = \bigcap_{i < m}N_i$ where $N_i\in \vec{M}_p\cup\left\{ M \right\}$. Let $b\in N$ and $x\in b\cap\mathrm{dom}f_p\backslash N$ then $\exists j < m$ s.t. $x\notin N_i$ and $b \in N_i$, thus from condition (\ref{condition:2}) $b$ would have already been in $\mathrm{dom}f_p$ satisfying $f_p\left( b \right) = f_p\left( x \right)$ ($N_i \neq M$ since $\mathrm{dom}f_p \subset M$).
\end{proof}

By lemma \ref{lem:why_d_M_is_a_condition}, if $M\in \vec{M}_d$ then $\vec{M}_d\cap M = res_M\left( \vec{M}_d \right)$, making $d^M$ a condition in $\mathbb{S}$ as well. Assume that $\left| f_d \right|= n$ and consider the set of conditions

\begin{equation*}
    \mathfrak{D} = \left\{ r\in D\ \Big|\ r\leq d^M\wedge \left| f_r \right|= n \right\}
\end{equation*}

Notice that $\mathfrak{D}$ is definable in $M$ hence $\mathfrak{D}\in M$.

\begin{remark}
    The assumption that $M\in \vec{M}_d$ is important, since then $d^M$ is a condition (lemma \ref{lem:why_d_M_is_a_condition}) and $d\leq d^M$, thus, $d\in \mathfrak{D}$ a fact which will later be used.
\end{remark}

\begin{definition}
    Let $X\subset T$, we say $X$ is \underline{unbounded} if $\sup\left\{ level\left( x \right)\ \Big|\ x\in X \right\} = \omega_1$.
\end{definition}

\begin{lemma}
    \label{lem:X_is_unbounded}
    Let $X\in M,\ X\subset T$ and assume $\exists x\in X\backslash M$. Then $X$ is unbounded.
\end{lemma}

\begin{proof}
    Let $X$ be such a set, we choose some element in $X\backslash M$ and denote it by $y$. Assume $X$ is bounded in $T$. Let $\alpha = \sup \left\{ level\left( x \right)\ \Big|\ x\in X \right\}$, since $X$ is bounded $\alpha < \omega_1$. Since $M\prec H(\omega_3)$ and $X\in M$ then $M$ also thinks that $X$ is bounded. Let $\sup \left\{ level(x)\ \Big|\ x\in X \right\} =\beta <\omega_1^M$.  Since $y\notin M$ and $y\in X$ then $\exists x\in X$ s.t. $level\left( x \right) >\omega_1^M > \beta$. Thus

    \begin{equation*}
        H\left( \omega_2 \right) \models \exists x\in X, level(x) > \beta
    \end{equation*}

    And from elementarity
    \begin{equation*}
        M \models \exists x\in X, level(x) > \beta
    \end{equation*}
    contradiction.
\end{proof}

\begin{corollary}
    The set $\bigcup\left\{\mathrm{dom}f_r\cap T \ \Big|\ r\in\mathfrak{D}  \right\}$ is unbounded in $T$.
\end{corollary}

\begin{proof}
    Using the lemma with $X = \bigcup\left\{\mathrm{dom}f_r\cap T \ \Big|\ r\in\mathfrak{D}  \right\}$.
\end{proof}

\begin{remark}
    \label{rem:reasons_for_incompatibility}
    Since $\forall r\in M\cap D,\ r\perp d$ the same holds for all conditions in $\mathfrak{D}\cap M$. Since $M\in \vec{M}_d$ and $\forall r\in \mathfrak{D}\cap M,\ r\leq d^M$, it follows $\vec{M}_r \leq res_M\left( \vec{M}_d \right)$ hence by lemma \ref{lem:q_leq_res} $\vec{M}_r \parallel \vec{M}_d$.

    Thus, every condition in $\mathfrak{D}$ has a compatible model sequence part to the model sequence of $d$, this means that \underline{the incompatibility can't come from the model sequence}.

    So, what can cause this incompatibility? Let $p\in \mathfrak{D}$

    \begin{enumerate}
        \item \label{incompatibility:1} Incompatibility of monotonicity:
              \begin{equation*}
                  \exists x \in \mathrm{dom}f_p\cap T,\ \exists y\in \mathrm{dom}f_d\cap T,\ x < _T y\wedge f_p\left( x \right) > f_d\left( y \right)
              \end{equation*}
              or
              \begin{equation*}
                  \exists x \in \mathrm{dom}f_p\cap T,\ \exists b\in \mathrm{dom} f_d \cap \left[ T \right],\ x \in b \wedge f_p\left( x \right) > f_d\left( b \right)
              \end{equation*}
              Notice that switching the direction of the inequality (i.e., with the roles of $p,d$ reversed), always leads to compatibility since if $b\in \mathrm{dom}f_p$ then $b\in M$ and if $x\in \mathrm{dom}f_d\backslash M$ from condition (\ref{condition:2}), $b\in \mathrm{dom}f_d$ hence in $\mathrm{dom}f_{d^M}$ and since $f_p \leq f_{d^M}$ it follows that $f_d\left( x \right) = f_d\left( b \right) = f_p\left( b \right)$.

        \item \label{incompatibility:2}$\exists N \in \vec{M}_d$ s.t. $N\notin M$, $\exists b\in N$, $\exists x_1, x_2\in b\cap\mathrm{dom}f_p\backslash N$ s.t. $x_1 < x_2$ and $f_p\left( x_1 \right) < f_p\left( x_2 \right)$. Here the incompatibility comes from a model $N$ unknown to $M$, s.t. $N\cap\omega_1 < M\cap\omega_1$, and $\mathrm{dom}\: f_d$ has a branch from $N$, this branch limits the possible values of $x_1,x_2$ (condition (\ref{condition:2})), but being unknown to $M$, $f_p$ sets $f_p\left( x_1 \right) < f_p\left( x_2 \right)$.

        \item \label{incompatibility:3}$\exists N\in \vec{M}_d$ s.t. $N\notin M$, $\exists b_\star \in N\cap\mathrm{dom}f_d\cap\left[ T \right]$, $\exists x\in b_\star\cap\mathrm{dom}f_p\backslash N$ s.t. $f_p\left( x \right) < f_d\left( b_\star \right)$. Here the incompatibility comes from a model $N$ unknown to $M$, s.t. $N\cap\omega_1 < M\cap\omega_1$ and $\mathrm{dom}\: f_d$ has a branch from $N$. This branch sets the value of some $x_1\in \mathrm{dom}f_p$ but being unknown to this $f_p$ it set it's value to be smaller the $f_d\left( b_\star \right)$. Notice that the case where the '$<$' changes to '$>$' is covered in (\ref{incompatibility:1}).
    \end{enumerate}
\end{remark}

Let $\left|\mathfrak{D}\right|=\zeta$. We want to fix an enumeration which is absolute between $H(\lambda)$ and $M$.

Since $M \models \left|\mathfrak{D}\right| = \zeta $, $\exists f \in M,\ f : \zeta \rightarrow \mathfrak{D}$ a bijection. Therefore, $H(\lambda)\models f:\zeta\rightarrow \mathfrak{D}$ is a bijection. This bijection now fixes an absolute enumeration of $\mathfrak{D}$.

Denote $f_\alpha = f_{p_\alpha}$ and consider $E = \left\{ a^\alpha\ \Big|\ \alpha < \left|\mathfrak{D}\right| \right\}$ where $a^\alpha$ is a choice of enumerations of $\mathrm{dom}f_{\alpha}$ for $p_\alpha\in\mathfrak{D}$ $\left( \mathrm{dom}f_p = \left\{ a^\alpha_0, \dots a^\alpha_n \right\} \right)$.

\begin{remark}
    The enumerations in $E\cap M$ are absolute between $H(\lambda)$ and $M$, since $M$ can describe $a^\alpha$ explicitly using a first order formula.
\end{remark}

\begin{claim}\label{claim:coloring}
    There exists a coloring (not-disjoint!) of $\zeta$, with the colors $P_{ij},P_{Nk_0k_1},P_{Nkb_\star}$ for $i,j,k, k_0,k_1 < n$, $N\in \vec{M}_d\wedge N\cap \omega_1 < M\cap\omega_1$ and $b_\star \in \mathrm{dom}f_d\cap\left[ T \right]$ with the following properties:

    \begin{equation*}
        \left(\alpha, \beta\in P_{ij}\rightarrow a^\alpha_i\parallel_T a^\beta_i\right)\wedge \left( \forall \alpha \in P_{ij},\ f_\alpha\left( a^\alpha_i \right) > f_d\left( d_j \right) \right)
    \end{equation*}
    \begin{equation*}
        \left(\alpha,\beta\in P_{Nkb_\star}\rightarrow a^\alpha_{k}\parallel a^\beta_{k}\right) \wedge \left( \forall\alpha\in P_{Nkb_\star}, level\left( a^\alpha_k \right) \geq N\cap\omega_1 \wedge f_\alpha \left( a^\alpha_k\right) < f_d\left( b_\star \right)\right)
    \end{equation*}
    and
    \begin{multline*}
        \left(\alpha, \beta \in P_{Nk_0k_1}\rightarrow \pi_{N\cap\omega_1}\left( a^\alpha_{k_1}\right)\neq\pi_{N\cap\omega_1}\left( a^\beta_{k_1} \right) \vee a^\alpha_{k_1} \parallel a^\beta_{k_1}\right)\\ \wedge \left( \forall \alpha \in P_{Nk_0k_1},\ level\left(a^\alpha_{k_0}\right) \geq N\cap\omega_1\wedge a^\alpha_{k_0} < a^\alpha_{k_1} \wedge f_\alpha\left( a^\alpha_{k_0}\right) < f_\alpha\left( a^\alpha_{k_1} \right) \right)
    \end{multline*}
\end{claim}

\begin{remark}
    Each color is supposed to capture a reason for an incompatibility with $d$. $P_{ij}$ captures reason \ref{incompatibility:1}, $P_{Nk_0k_1}$ captures reason \ref{incompatibility:2} and $P_{Nkb_\star}$ captures reason \ref{incompatibility:3}.
\end{remark}

\begin{proof}
    \renewcommand{\qedsymbol}{}
    Let $X\subset \zeta\cap M$ be a finite set. Consider the sets (defined in $H\left( \omega_3 \right)$).
    \begin{equation*}
        P_{ij}^X = \left\{ \alpha \in X\ \Big|\ a^\alpha_i < d_j \wedge f_\alpha\left( a^\alpha_i \right) > f_d\left( d_j \right)\right\}
    \end{equation*}

    \begin{equation*}
        P_{Nk_0k_1}^X = \left\{ \alpha\in X\ \Big|\ \psi_1\left( \alpha, N\cap\omega_1, k_0, k_1 \right)\wedge\exists b\in \left[ T \right]\cap\left( N\backslash M \right), a^\alpha_{k_1}\in b \right\}
    \end{equation*}

    \begin{equation*}
        P_{Nkb_\star}^X = \left\{ \alpha\in X\ \Big|\ \psi_2\left( \alpha, N\cap\omega_1, k, b_\star  \right)\wedge a^\alpha_k\in b_\star \right\}
    \end{equation*}

    where

    \begin{equation*}
        \psi_1\left( \alpha, N\cap\omega_1, k_0, k_1 \right) = level(a^\alpha_{k_0}) \geq N\cap\omega_1\wedge a^\alpha_{k_0} < a^\alpha_{k_1}\wedge f_\alpha(a^\alpha_{k_0}) < f_\alpha (a^\alpha_{k_1})
    \end{equation*}

    \begin{equation*}
        \psi_2\left( \alpha, N\cap\omega_1, k, b_\star \right) = level(a^\alpha_k)\geq N\cap \omega_1\wedge f_\alpha(a^\alpha_k) < f_d(b_\star)
    \end{equation*}

    and

    \begin{equation*}
        i,j,k, k_0,k_1 < n,\ \left(N\in \vec{M}_d\wedge N\cap \omega_1 < M\cap\omega_1\right),\ b_\star \in \mathrm{dom}f_d\cap\left[ T \right]
    \end{equation*}
\end{proof}

\begin{lemma}
    $P_{ij}^X,\ P_{Nk_0k_1}^X,\ P^X_{Nkb_\star}$ satisfy the properties listed in claim \ref{claim:coloring}.
\end{lemma}

\begin{proof}
    Let $\alpha,\beta \in X$.

    Assume $\alpha,\beta \in P_{ij}^X$ then $a^\alpha_i \parallel a^\beta_i$ since $a^\alpha_i,a^\beta_i < d_j$.

    Assume $\alpha,\beta \in P_{Nk_0k_1}^X$, if $\pi_{N\cap\omega_1}\left( a^\alpha_{k_1}\right) = \pi_{N\cap\omega_1}\left( a^\beta_{k_1} \right)$ then $\exists b_1,b_2\in \left[ T \right]\cap N\backslash M$ s.t. $a^\alpha_{k_1}\in b_1\wedge a^\beta_{k_1}\in b_2$. We claim that $N \models b_1 = b_2$, assume otherwise then $\exists \alpha < \omega_1^N$ s.t $b_1\cap T_\alpha\neq b_2\cap T_\alpha$ which is a contradiction to  $\pi_{N\cap\omega_1}\left( a^\alpha_{k_1}\right) = \pi_{N\cap\omega_1}\left( a^\beta_{k_1} \right)$. Since $N\models b_1 = b_2$ and $N\prec H(\lambda)$ then $H(\lambda)\models b_1 = b_2$. Hence, $a^\alpha_{k_1}$ and $a^\beta_{k_1}$ are on the same branch i.e. $a^\alpha_{k_1}\parallel a^\beta_{k_1}$.

    Assume $\alpha,\beta \in P_{Nkb_\star}^X$, then $a^\alpha_k,a^\beta_k\in b_\star$ hence $a^\alpha_k \parallel a^\beta_k$.

\end{proof}

\begin{lemma}
    $X = \bigcup P_{ij}^X\cup\bigcup P_{Nk_0k_1}^X\cup\bigcup P_{Nkb_\star}^X$
\end{lemma}

\begin{proof}
    Let $\alpha\in X$, since $X\subset E\cap M,\ p_\alpha\perp d$, from one (or more) of reasons \ref{incompatibility:1}, \ref{incompatibility:2}, \ref{incompatibility:3} for incompatibility of a condition.

    If the incompatibility comes from \ref{incompatibility:1} then there exists some index $i < n$ and $j < n$ s.t. $a^\alpha_i < d_j$ but $f_\alpha\left( a^\alpha_i \right) < f_d\left( d_j \right)$. In this case by definition $\alpha\in P_{ij}^X$.

    If the incompatibility comes from \ref{incompatibility:2} then $\exists N\in \vec{M}_d$ s.t. $N\notin M$ with some $b\in N$ and $x_1,x_2\in b\cap \mathrm{dom}f_\alpha\backslash N$ s.t. $x_1 < x_2$ and $f_\alpha\left( x_1 \right) < f_\alpha\left( x_2 \right)$. Since $\mathrm{dom}f_\alpha\cap T\backslash N \neq \emptyset$ and $p_\alpha\in M$ then $M\cap\omega_1 > N\cap\omega_1$ (corollary \ref{cor:M_cap_omega1_geq}). Let $k_0,k_1$ be the indices of $a^\alpha$ s.t. $a^\alpha_{k_0} = x_1\wedge a^\alpha_{k_1} = x_2$, then $\alpha \in P_{Nk_0k_1}^X$.

    If the incompatibility is from \ref{incompatibility:3} then $\exists N\in \vec{M}_d$ s.t. $N\notin M$ with some $b_\star \in N$ and $x_1\in b_\star\cap\mathrm{dom}f_\alpha \backslash N$ and $f_\alpha\left( x_1 \right) < f_d\left( b_\star \right)$. Let $k$ be the index s.t. $a^\alpha_{k} = x_1$ then $\alpha\in P_{Nkb_\star}^X$.

    Thus, $X = \bigcup P_{ij}^X\cup\bigcup P_{Nk_0k_1}^X\cup\bigcup P_{Nkb_\star}^X$.
\end{proof}

\begin{proof}[Back to the proof of claim \ref{claim:coloring}]
    \renewcommand{\qedsymbol}{}
    We showed that we can construct a coloring in $H(\lambda)$ of every finite $X\subset E\cap M$, satisfying the properties listed in claim \ref{claim:coloring}.

    Let $X\subset \zeta\cap M$, since

    \begin{equation*}
        H(\lambda) \models \mathrm{There\ is\ a\ coloring\ of\ } X\ \mathrm{with\ the\ properties\ listed\ in\ claim\ \ref{claim:coloring}}
    \end{equation*}

    Hence, since $M'\prec H(\lambda)$ and $M = M'\cap H(\omega_2)$

    \begin{equation*}
        M \models \mathrm{There\ is\ a\ coloring\ of\ } X\ \mathrm{with\ the\ properties\ listed\ in\ claim\ \ref{claim:coloring}}
    \end{equation*}

    Let $P_{ij}^{X,M},P_{Nk_0k_1}^{X,M},P_{Nkb_\star}^{X,M}\in M$ be this coloring of $X$ inside $M$. Next we will use the compactness theorem to get a coloring of all of $\zeta^M$.
\end{proof}

\begin{remark}
    \label{rem:why_in_M}
    For the reflection argument above to work every variable in the definition of the properties in claim \ref{claim:coloring} needs to be in $M$.

    Notice that in $P_{ij}$ the definition uses $f_d(d_j)\in\omega$ which is inside $ M$. In $P_{Nk_0k_1}$ the definition uses $N\cap\omega_1$ which is in $M$ since $N\cap\omega_1 <M\cap\omega_1$. And in $P_{Nkb_\star}$ the definition uses $N\cap\omega_1$ and $f_d(b_\star)$ in the same manner as in $P_{ij}$.
\end{remark}

\begin{proof}[Back to the proof of claim \ref{claim:coloring}]
    \renewcommand{\qedsymbol}{}
    Add $\zeta$ constant symbols $c_\alpha$ to our language $(\in)$ and another finite amount of constants $\widetilde{P}_{ij},\widetilde{P}_{Nk_0k_1},\widetilde{P}_{Nkb_\star}$.
    Define the following theories in $M$

    \begin{equation*}
        \mathfrak{T}_0 = \left\{ c_\alpha\notin \widetilde{P}_{ij} \vee c_\beta\notin \widetilde{P}_{ij}\ \Big|\ \alpha,\beta < \zeta,\ i, j< n,\ a^\alpha_i\perp a^\beta_i\right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_1 = \left\{ c_\alpha \notin \widetilde{P}_{ij}\ \Big|\ \alpha <\zeta,\ i,j < n,\ f_\alpha(a^\alpha_i) < f_d(d_j) \right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_2 = \left\{ c_\alpha\notin \widetilde{P}_{Nk_0k_1} \vee c_\beta\notin \widetilde{P}_{Nk_0k_1}\ \Big|\ \alpha <\zeta,\ \pi_{N\cap\omega_1}\left( a^\alpha_{k_1}\right) = \pi_{N\cap\omega_1}\left( a^\beta_{k_1} \right)\wedge a^\alpha_{k_1}\perp a^\beta_{k_1} \right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_3 = \left\{ c_\alpha \notin \widetilde{P}_{Nk_0k_1}\ \Big|\ \alpha <\zeta,\ level(a^\alpha_{k_0}) < N\cap\omega_1\vee a^\alpha_{k_0} < a^\alpha_{k_1}\vee f_\alpha(a^\alpha_{k_0})\geq f_\alpha(a^\alpha_{k_1}) \right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_4 = \left\{ c_\alpha\notin \widetilde{P}_{Nkb_\star} \vee c_\beta\notin \widetilde{P}_{Nkb_\star}\ \Big|\ \alpha < \zeta,\ \pi_{N\cap\omega_1}(a^\alpha_{k}) = \pi_{N\cap\omega_1}(a^\beta_k)\wedge a^\alpha_k \perp a^\beta_k \right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_5 = \left\{ c_\alpha\notin \widetilde{P}_{Nkb_\star}\ \Big|\ \alpha <\zeta,\ level(a^\alpha_k) < N\cap\omega_1\vee f_\alpha(a^\alpha_k)\geq f_d(b_\star) \right\}
    \end{equation*}
    \begin{equation*}
        \mathfrak{T}_6 = \left\{ \bigvee_{ij}c_\alpha \in \widetilde{P}_{ij}\vee\bigvee_{Nk_0k_1}c_\alpha \in \widetilde{P}_{Nk_0k_1} \vee \bigvee_{Nkb\star}c_\alpha\in\widetilde{P}_{Nkb_\star}\ \Big|\ \alpha <\zeta\right\}
    \end{equation*}

    Consider $\mathfrak{T} = \bigcup_i\mathfrak{T}_i$
\end{proof}

\begin{lemma}
    $M\models \mathfrak{T}$ is finitely satisfiable.
\end{lemma}

\begin{proof}
    Since all variables used to define $\mathfrak{T}$ are in $M$ (remark \ref{rem:why_in_M}) $\mathfrak{T}$ is definable in $M$ hence $\mathfrak{T}\in M$.
    Let $\mathcal{X}\subset\mathfrak{T}$ be finite. Let $X = \left\{ \alpha\ \Big|\ c_\alpha \mathrm{\ is\ in\ a\ formula\ of\ }\mathcal{X} \right\}$. Interpret $c_\alpha$ as $\alpha$ and $\widetilde{P}_{ij} = P_{ij}^{X,M},\ \widetilde{P}_{Nk_0k_1} = P_{Nk_0k_1}^{X,M},\ \widetilde{P}_{Nkb_\star} = P_{Nkb_\star}^{X,M}$. By the previous argument this satisfies $\mathcal{X}$. Since this is true for every finite $\mathcal{X}\subset\mathfrak{T}$, $\mathfrak{T}$ is finitely satisfiable.
\end{proof}

\begin{proof}[Back to the proof of claim \ref{claim:coloring}]
    Since $\mathfrak{T}$ is finitely satisfiable, from compactness it is satisfiable. Let $P_{ij},\ P_{Nk_0k_1},\ P_{Nkb_\star}$ be the sets which satisfy it. Thus, $P_{ij},\ P_{Nk_0k_1},\ P_{Nkb_\star}$ color all of $\zeta$ in $M$ i.e.
    \begin{equation*}
        M\models P_{ij},P_{Nk_0k_1},P_{Nkb_\star}\  is\ a\ coloring\ of\ \zeta\ with\ the\ properties\ listed\ in\ \ref{claim:coloring}
    \end{equation*}
    hence
    \begin{equation*}
        H(\lambda)\models P_{ij},P_{Nk_0k_1},P_{Nkb_\star}\  is\ a\ coloring\ of\ \zeta\ with\ the\ properties\ listed\ in\ \ref{claim:coloring}
    \end{equation*}
\end{proof}

Denote by $P_{ij}^\mathfrak{D},P_{Nk_0k_1}^\mathfrak{D},P_{Nkb_\star}^\mathfrak{D}$ the set of conditions, $P^\mathfrak{D} = \left\{ p_\alpha\in \mathfrak{D}\ \Big|\  \alpha\in P\right\}$. Then there exists some color $P$ with $d\in P^\mathfrak{D}$.

\begin{notation}
    For condition $p$, if indices $i, j$ are not the reason for incompatibility with $d$ in the sense of incompatibility reason \ref{incompatibility:1} we write $p \parallel_{(1)_{ij}}d$. Similarly, with incompatibility reasons \ref{incompatibility:2} and \ref{incompatibility:3}.
\end{notation}

\begin{claim}
    \label{claim:parallel_1}
    If $d\in P_{ij}^\mathfrak{D}$ then $\exists X\subset P_{ij}^\mathfrak{D},\ X\in M $ s.t. $d\in X$ and $\forall p \in X\cap M,\, p\parallel_{(1)_{ij}} d$.
\end{claim}

\begin{proof}
    Suppose $d\in P_{ij}^\mathfrak{D}$ for some $i,j < n$. If $d_i \in M$, take the set

    \begin{equation*}
        X = \left\{ \alpha \in P_{ij}\, \Big|\, a^\alpha_i = d_i \right\}
    \end{equation*}

    Since $f_d(d_i) > f_d(d_j)$ and $d_i,d_j\in \mathrm{dom}f_d$ then $d_i \nleq d_j$ proving every condition in $X$ is $\parallel_{(1)_{ij}}$ to $d$, and because $d_i\in M$, $X$ is definable in $M$ i.e. $X\in M$.

    If $d_i \notin M$ then the set $Y = \left\{ a^\alpha_i\ \Big|\ \alpha\in P_{ij} \right\}$ is unbounded since $d_i \in Y\wedge d_i\notin M$ by lemma \ref{lem:X_is_unbounded}. Moreover, $\forall x,y\in Y,\ x\parallel y$. $Y$ then defines a branch via
    \begin{equation*}
        B = \bigcup\left\{ seg(x)\ \Big|\ x\in Y \right\}
    \end{equation*}

    This branch goes through $d_i$. Since $Y$ is definable in $M$, $Y\in M$ and using $Y$ we can build $B$ thus $B\in M$ then by condition (\ref{condition:2}) $B\in \mathrm{dom}f_d$ and $f_d(B) = f_d(d_i)$. We consider $d_i\wedge d_j\in \mathrm{dom}f_d$. If $d_i \wedge d_j\notin M$ then we get a contradiction since

    \begin{equation*}
        f_d(B) = f_d(d_i\wedge d_j) \leq f_d(d_j) < f_d(d_i) = f_d(B)
    \end{equation*}

    If $d_i\wedge d_j\in M$ then the set

    \begin{equation*}
        X = \left\{ p\in P_{ij}^\mathfrak{D}\ \Big|\, \forall x\in \mathrm{dom}f_p\backslash \mathrm{dom}f_{d^M},\, level(x) > level(d_i\wedge d_j) \right\}
    \end{equation*}

    Will satisfy the requirement. First $d\in X$ since $\forall x\in \mathrm{dom}f_d\backslash \mathrm{dom}f_{d^M}$, $level(x) \geq  M\cap\omega_1$ and since $d_i\wedge d_j\in M,\, level(d_i\wedge d_j) < M\cap\omega_1$. Let $p\in X\cap M$, then the $i^{th}$ element is above $d_i\wedge d_j$ (by definiton) and below $d_i$ ($d_i \notin M$) hence it can't be incompatible because of incompatibility reason number (\ref{incompatibility:1}) for the indices $ij$.
\end{proof}

\begin{claim}
    \label{claim:parallel_2}
    If $d\in P_{Nk_0k_1}^\mathfrak{D}$ then $\exists X\subset P_{Nk_0k_1}^\mathfrak{D},\ X\in M $ s.t. $d\in X$ and $\forall p \in X\cap M,\, p \parallel_{(2)_{Nk_0k_1}} d$.
\end{claim}

\begin{proof}
    Suppose $d\in P_{Nk_0k_1}^\mathfrak{D}$ for some $N,k_0,k_1$ and suppose $d_{k_0}\in M$. If $d_{k_1}\notin M$ then

    \begin{equation*}
        Y = \left\{ a^\alpha_{k_1}\, \Big|\, \alpha\in P_{Nk_0k_1},\ \pi_{N\cap\omega_1}(a^\alpha_{k_1}) = \pi_{N\cap\omega_1}(d_{k_1}) \right\}
    \end{equation*}

    defines a branch $B$ going through $d_{k_1}$ then by condition \ref{condition:2}, $B\in \mathrm{dom}f_d$. Consider the set

    \begin{equation*}
        X = \left\{ \alpha \in P_{Nk_0k_1}\, \Big|\, a^\alpha_{k_0} = d_{k_0}\right\}
    \end{equation*}

    Then $d\in X$ and taking the subset

    \begin{equation*}
        X' = \left\{ \alpha\in  X\,  \Big|\, \forall x\in \mathrm{dom}f_{p_\alpha}\backslash \mathrm{dom}f_{d^M},\ level(x) > level(d_{k_0}) \right\}
    \end{equation*}

    Assume $\exists b\in N\cap\left[ T \right]\cap\mathrm{dom}f_d$ and $\alpha\in X'$ s.t. $a^{\alpha}_{k_1}\in b$ then $d_{k_0} < a^\alpha_{k_1} < B\wedge b\in \mathrm{dom}f_d$. Notice that there can be at most one branch satisfying this, since the projection on $N\cap\omega_1$ is the same for all branches going through $d_{k_0}$. If $B\wedge b \notin M$ then from condition \ref{condition:2}, $ f_d(d_{k_1} )= f_d(B) = f_d(b) = f_d(d_{k_0})$ which is a contradiction. Hence, $B\wedge b \in M$, consider the set

    \begin{equation*}
        X'' = \left\{ \alpha\in  X,  \Big|\, \forall x\in \mathrm{dom}f_{p_\alpha}\backslash \mathrm{dom}f_{d^M},\ level(x) > level(B\wedge b) \right\}
    \end{equation*}

    By definition $d \in X''$, and for all $b'\in N$ and for all $p_\alpha\in X''$ we have that $a^\alpha_{k_1} \notin b'$. Hence, $X''$ is the required set.

    If $d_{k_1}\in M$ then taking
    \begin{equation*}
        X = \left\{\alpha\in P_{Nk_0k_1}\, \Big|\, a^\alpha_{k_0} = d_{k_0}\wedge a^\alpha_{k_1} = d_{k_1}\right\}
    \end{equation*}

    Gives the required set. If $d_{k_0}\notin M$, we define

    \begin{equation*}
        Y = \left\{ a^\alpha_{k_1}\ \Big|\  \alpha \in P_{Nk_0k_1}\wedge \pi_{N\cap\omega_1}\left( a^\alpha_{k_1} \right) = \pi_{N\cap\omega_1}\left( d_{k_1} \right) \right\}
    \end{equation*}

    Since $d_{k_1}\in Y$ and $d_{k_1}\notin M$, by lemma \ref{lem:X_is_unbounded} $Y$ is unbounded and defines a branch

    \begin{equation*}
        B = \bigcup\left\{ seg(x)\ \Big|\ x\in Y \right\}
    \end{equation*}

    This branch goes through $d_{k_1}$ therefore also through $d_{k_0}$ and we get a contradiction since

    \begin{equation*}
        f_d(B) = f_d(d_{k_0}) < f_d(d_{k_1}) = f_d(B)
    \end{equation*}
\end{proof}

\begin{claim}
    \label{claim:parallel_3}
    If $d\in P_{Nkb_\star}^\mathfrak{D}$ then $\exists X\subset P_{Nkb_\star}^\mathfrak{D},\ X\in M $ s.t.  $d\in X$ and $\forall p \in X\cap M,\,  p \parallel_{(3)_{Nkb_\star}} d$.
\end{claim}

\begin{proof}
    Suppose $d\in P_{Nkb_\star}^\mathfrak{D}$ for some $N,k,b_\star$ and suppose $d_k\in M$ consider the set

    \begin{equation*}
        X = \left\{\alpha \in P_{Nkb_\star}\, \Big|\, a^\alpha_k = d_k \right\}
    \end{equation*}

    By the same arguments this is the required set.

    If $d_k\notin M$ then the set $\left\{ a^\alpha_k\, \Big|\, \alpha \in P_{Nkb_\star} \right\}$ defines a branch $B\in M$ going through $d_k$ and by condition (\ref{condition:2}), $B\in \mathrm{dom}f_d$
    \begin{equation*}
        f_d(B) = f_d(d_k) < f_d(b_\star)
    \end{equation*}

    Consider $b_\star\wedge B\in \mathrm{dom}f_d$. If $b_\star\wedge B\notin M$ then

    \begin{equation*}
        f_d(B) = f_d(B\wedge b) = f_d(b)
    \end{equation*}

    Contradiction. Hence, $b_\star\wedge B\in M$ and the set

    \begin{equation*}
        X = \left\{p \in P_{Nkb_\star}^\mathfrak{D}\, \Big|\, \forall x\in \mathrm{dom}f_p\backslash \mathrm{dom}f_{d^M}, x \parallel b_\star\wedge B\rightarrow b_\star\wedge B < x\right\}
    \end{equation*}

    contains $d$. Let $p\in X$ then since every new element of $\mathrm{dom}f_p$ which can cause incompatibility (from reason \ref{incompatibility:3}) is \underline{above} the point where the branches $B$ and $b_\star$ diverge thus can't be the cause for the incompatibility.
\end{proof}

\begin{claim}
    \label{claim:p_parallel_d}
    $\exists p\in \mathfrak{D}$ s.t. $p\parallel d$.
\end{claim}

\begin{proof}
    Since

    \begin{equation*}
        \mathfrak{D} = \bigcup P_{ij}^\mathfrak{D}\cup \bigcup  P_{Nk_0k_1}^\mathfrak{D} \cup \bigcup P_{Nkb_\star}^\mathfrak{D}
    \end{equation*}
    $\exists P^\mathfrak{D}$ a color s.t. $d\in P^\mathfrak{D}$. By claims \ref{claim:parallel_1}, \ref{claim:parallel_2}, \ref{claim:parallel_3} there exists $X\subset P\wedge X\in M$ s.t. $d\in X$ and $\forall x\in X\cap M$ there is one less incompatibility reason between $x$ and $d$ (depends on the color $P$). Employing this argument repeatedly gives, after a finite number of steps, a set $X\in M$ s.t. $d\in X$ and $\forall x\in X\cap M,\, x\parallel d$. Since $d\in X$, $M$ thinks $X\neq\emptyset$ thus $\exists p\in X\cap M$.
\end{proof}

\begin{proof}[Proof of Theorem \ref{thm:P_is_proper}]
    By claim \ref{claim:p_parallel_d}, let $p\in \mathfrak{D}\cap M$ be a condition s.t. $p\parallel d$. And we get a contradiction to the assumption that all conditions in $\mathfrak{D}\cap M$ are incompatible with $d$.
\end{proof}


\begin{theorem}
    $\mathbb{S}$ is proper for $\mathcal{T}$.
\end{theorem}

\begin{proof}
    Even though this case is essentially covered by the proof of \ref{thm:P_is_proper} we present it because of its simplicity. Let $W\in \mathcal{T}$, again we go toward contradiction, the difference here is that by defining $\mathfrak{D}$ intelligently we can avoid all reasons for incompatibility listed in remark \ref{rem:reasons_for_incompatibility}. Define

    \begin{equation*}
        \mathfrak{D} = \left\{ r\in D\, \Big|\, r\leq d^W \wedge |\vec{M}_r| = |\vec{M}_d|\wedge |f_r| = |f_d|\wedge |\mathrm{dom}\: f_r\cap[T]| = |\mathrm{dom}\: f_d\cap[T]|\right\}
    \end{equation*}

    Since $d\in\mathfrak{D}$ this set is non-empty, and since $d\notin W$ then $\mathfrak{D}\cap W \neq \emptyset$. We claim that all conditions in $D\cap W$ are compatible to $d$, to do that we need to show all reasons for incompatibility are not applicable here. The argument is simple, since $T\subset W$ and $|f_r| = |f_d|\wedge |\mathrm{dom}\: f_r\cap[T]| = |\mathrm{dom}\: f_d\cap[T]|$, the conditions in $\mathfrak{D}$ can add only branches. Now taking any $p\in \mathfrak{D}\cap W$ leads to a contradiction.
\end{proof}