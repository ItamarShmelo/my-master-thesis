\section{Preliminaries}

This section provides the necessary  definitions, notations, lemmas and theorems we use in the proofs in this paper. The rest of the paper is mostly self-contained granted one has a basic knowledge of forcing and (set theory).

The definitions are taken from \cite{Jech} with minor notational changes.

\begin{definition}
    An ordered pair $\left\langle T,\leq_{T}\right\rangle $ is a tree
    if for all $x\in T$ the set $\left\{ y\in T\ \big|\ y<_{T}x\right\} $
    is well-ordered by $\leq_{T}$.
\end{definition}

\begin{remark}
    From now on when we refer to a tree we omit the ordered pair and denote it by $T$, where the $\leq_{T}$
    is implicit.
\end{remark}


\begin{definition}
    Let $T$ be a tree.

    Let $x\in T$, we denote the level of $x$ in $T$ by $level\left(x\right)=\mathrm{otp}\left(\left\{ y\in T\ \big|\ y<_{T}x\right\} \right)$.

    For $\alpha$ an ordinal, we denote the $\alpha$ level of the tree
    by $T_{\alpha}=\left\{ x\in T\ \big|\ level\left(x\right)=\alpha\right\} $

    The height of the tree $T$ is the least ordinal $\alpha$ s.t. $T_{\alpha}=\emptyset$.

    For $x\in T$ the initial segment of $x$ is the set $seg(x) = \left\{ y\in T\ \Big|\ y\leq_T x \right\}$
\end{definition}

\begin{definition}
    Let $T$ be a tree and let $x,y\in T$, define the \underline{highest
        common ancestor} of $x,y$ as the element $r\in T$ s.t. $r\leq x,y$
    and $\forall t\in T,$ if $r < t$ then $t\nleq x\vee t\nleq y$. We denote
    this element by $x\wedge y$.
\end{definition}

\begin{lemma}
    \label{lem:wedge_properties}
    The following properties hold for the $\wedge$ operator:

    \begin{enumerate}
        \item \label{wedge_property_1} For $x,y\in T$ if $x\leq y$ then $x\wedge y = x$.
        \item \label{wedge_property_2} For $x,y,z\in T$ if $x\leq y$ then $z\wedge x\leq z\wedge y$
        \item \label{wedge_property_3} For $x,y,z \in T$ if $x\leq y$ and $x\nleq z$ then $x\wedge z = y\wedge z$.
    \end{enumerate}

\end{lemma}

\begin{proof}
    \hfill
    \begin{enumerate}
        \item Let $x,y\in T$ s.t. $x\leq y$ then $x\wedge y \leq x, y$ but since $x\leq x,y$ then $x \leq x\wedge y$ hence $x\wedge y = x$.
        \item Let $x,y,z\in T$ s.t. $x\leq y$. $z\wedge x\leq x,z$ hence $z\wedge x\leq y,z$ i.e. $z\wedge x \leq z\wedge y$.
        \item Let $x,y,z\in T$ s.t. $x\leq y$ and $x\nleq z$ then $x\wedge z\leq y\wedge z$ since $x\leq y$. Assume $x \leq z\wedge y$ then $x\leq z$ contradiction.
    \end{enumerate}
\end{proof}

\begin{definition}
    Let $\kappa$ be a regular cardinal. $T$ is called a $\kappa$-tree
    if $T$ is a tree of height $\kappa$ and $\left|T_{\alpha}\right|<\kappa$
    for all $\alpha<\kappa$.
\end{definition}

\begin{definition}
    Let $T$ be a $\kappa$-tree, a branch of $T$ is a set $b\subset T$
    s.t. $b$ is linearly ordered and $T_{\alpha}\cap b\neq\emptyset$
    for all $\alpha<\kappa$.

    The set of branches of $T$ is denoted by

    \begin{equation*}
        \left[T\right] \equiv \left\{ b\subset T\ \bigg|\ b\mathrm{\ is\ a\ branch}\right\}
    \end{equation*}
\end{definition}

\begin{definition}
    An $\omega_1$-tree with not branches is called an Aronszajn tree.
\end{definition}

\begin{theorem}[Baumgartner \cite{JechBaumgartner}]
    Let $T$ be an Aronszajn tree, then there exists a c.c.c. forcing that adds a specializing function $f:T\rightarrow\omega$ using finite approximations.
\end{theorem}

\begin{definition}
    A tree $T$ is called a Kurepa tree if $T$ is an $\omega_{1}$-tree
    and $\left|\left[T\right]\right|=\aleph_{2}$.
\end{definition}

\begin{theorem}[Solovay \cite{JechSolovay}]
    \label{thm:solovay}
    If $V = L$, then there exists a Kurepa tree.
\end{theorem}

\begin{notation}
    A forcing notion is denoted by $\mathbb{P},\mathbb{Q},\mathbb{R}$ etc.
\end{notation}

\begin{notation}
    If $x,y\in \mathbb{P}$ and $\exists r\in \mathbb{P}$ s.t. $r\leq_{\mathbb{P}} x,y$ we denote $x\parallel y$. For a tree $T$ this translates to, $x, y\in T$ then $x\parallel y \iff x \leq_T y\vee y\leq_T x$.
\end{notation}

\begin{definition}
    Let $T$ be a tree, we define the tree-forcing $\mathbb{T} = T$ and $\leq_T\: =\: \geq_{\mathbb{T}}$ (reverse order).
\end{definition}

\begin{definition}[Levy Collapse]
    Let $\mu$ be a regular cardinal and $\kappa$ a strongly inaccessible cardinal s.t. $\mu < \kappa$. Define the Levy Collapse $\mathbb{C}_{\mu, <\kappa}$:
    $p\in \mathbb{C}_{\mu, <\kappa}$ is a partial function $p:\kappa\times \mu\rightarrow\kappa$ s.t.
    \begin{enumerate}
        \item $|p| < \mu$, and
        \item $\forall \alpha <\kappa\forall \beta < \mu $ if $\left( \alpha,\beta \right)\in p$ then $p(\alpha,\beta) <\alpha$.
    \end{enumerate}
    $\mathbb{C}_{\mu, <\kappa}$ is ordered by reverse inclusion.
\end{definition}

Let $\kappa$ be a cardinal.

\begin{definition}
    A forcing notion $\mathbb{P}$ is called $\kappa$-closed iff for every sequence of conditions $\left< p_\alpha\: \bigg|\: \alpha <\delta \right>$ for $\delta < \kappa $ and

    \begin{equation*}
        p_\beta \leq p_\alpha,\quad \alpha < \beta
    \end{equation*}

    there exists a condition $p\in \mathbb{P}$ s.t. $\forall\alpha <\delta,\:  p \leq p_\alpha$. We use $\sigma$-closed instead of $\omega_1$-closed.
\end{definition}

\begin{definition}
    A forcing notions is said to be $\kappa$-c.c. if the cardinality of every anti-chain is less than $\kappa$. We use c.c.c. for $\omega_1$-c.c.
\end{definition}

\begin{definition}
    A forcing notion $\mathbb{P}$ is $\kappa$-distributive iff for every collection of open dense sets $\left< D_\alpha\: \bigg|\: \alpha <\delta\right>$ for $\delta <\kappa$ the intersection $\bigcap_{\alpha <\delta}D_\alpha$ is dense.
\end{definition}

\begin{lemma}
    \label{lem:distributive_no_reals}
    Let $\mathbb{P}$ be an $\omega_1$-distributive forcing-notion, then $\mathbb{P}$ does not add new reals.
\end{lemma}

\begin{lemma}
    \label{lem:T_height_in_M}
    Let $T$ be an $\omega_1$-tree and let $M$ be a countable elementary submodel with $T\in M$. Then
    \begin{equation*}
        T\cap M = T\restriction M\cap\omega_1
    \end{equation*}
\end{lemma}

\begin{proof}
    $M \models T\ \mathrm{is\ an\ }\omega_1\mathrm{- tree}$. Thus, $T_\alpha \in M$ for all $\alpha < \omega_1\cap M$ since it is definable in $M$ using $T$. Since $M\models \left| T_\alpha\right| = \aleph_0$ there exists in $M$ a bijection $f:\omega\rightarrow T_\alpha$. Since $M$ is an elementary submodel $f$ is a bijection in the model. Since $\forall n < \omega, f(n)\in M$ and $f$ is a bijection it follows that $T_\alpha \subset M$.
\end{proof}

\begin{corollary}
    \label{cor:M_cap_omega1_geq}
    If $M,N$ are two countable elementary submodels and $T\in M,N$ and $\exists x \in T\cap M \backslash N$ then $M\cap\omega_1 > N\cap\omega_1$.
\end{corollary}

\begin{corollary}
    Assuming $V = L$, $\mathbb{T}$ adds a branch to Solovays Kurepa tree (theorem \ref{thm:solovay}) without collapsing $\omega_1$ or $\omega_2$.
\end{corollary}

\begin{proof}
    We present briefly the construction of the tree (without proof that it is Kurepa). Define a function

    \begin{equation*}
        f(\alpha) = \min \left\{ \gamma\, \bigg|\, \alpha \in L_\gamma \prec L_{\omega_1} \right\}
    \end{equation*}

    Using this $f$ we define a Kurepa family (namely, a family of size $\aleph_2$ with countable restrictions).

    \begin{equation*}
        \mathcal{F} = \left\{ X\subset \omega_1\, \bigg|\, \forall
        \alpha <\omega_1, X\cap \alpha \in L_{f(\alpha)} \right\}
    \end{equation*}

    Using this family we define a Kurepa tree constructed from the functions $g_X(\alpha) = X\cap\alpha$

    \begin{equation*}
        T = \left\{ g_X\restriction\beta\, \bigg|\, \beta <\omega_1 \right\}
    \end{equation*}

    Let $\mathbb{T}$ be the tree forcing, we want to show $\mathbb{T}$ is $\omega_1$-distributive. Let $\vec{D} = \left< D_n\: \bigg|\: n <\omega\right>$ be some collection of open dense sets and let $M\prec L_{\omega_2}$ be a countable elementary submodel s.t. $T, \vec{D}\in M$. Let $\pi: M\rightarrow \overline{M}$ be the transitive collapse and suppose $M\cap\omega_1 = \gamma$. Since

    \begin{equation*}
        M\models D_n \text{ are open dense} \Longrightarrow \overline{M}\models \pi(D_n)\text{ are open dense}
    \end{equation*}

    by \ref{lem:T_height_in_M} $\pi(T) = T\restriction\gamma$ and from the condensation lemma

    \begin{equation*}
        \exists\zeta<\omega_1,\: \overline{M} = L_\zeta\models\gamma = \omega_1\wedge V = L
    \end{equation*}

    Since $\gamma \in L_\zeta$, we claim that $L_\zeta\trianglelefteq L_{f(\gamma)}$. Since $L_{f(\gamma)}\prec L_{\omega_1}$ and $L_{\omega_1}\models |\gamma| = \aleph_0$ we have that $L_{f(\gamma)} \models |\gamma|=\aleph_0$ thus $L_{f(\gamma)}$ is higher in the hierarchy. Thus $L_\zeta\trianglelefteq L_{f(\gamma)}$ and $L_\zeta\in L_{f(\gamma)}$. Let $t_0\in L_{f(\gamma)}\cap \mathbb{T}$. Take $\overline{t}_{n + 1}\in \pi(D_n)$ be the first in the $\leq_L$ ordering s.t. $\overline{t}_{n + 1} \leq \overline{t}_n$. Define $t_n = \pi^{ -1}(\overline{t}_n)$ then $t_{n + 1}\in D_n$. Since $\pi(\vec{D})\in \overline{M}\subset L_{f(\gamma)}$ the sequence $\left< \overline{t}_n\, \big|\, n <\omega\right>\in \overline{M}$. Remember that $\pi(t) = t$, thus $\left< t_n\, \big|\, n <\omega\right>\in \overline{M}$, denote $t_n = g_{X_n}\restriction\alpha_n$ and consider

    \begin{equation*}
        \bigcup g_{X_n}\restriction \alpha_n = g_{\bigcup X_n\cap \alpha_n}
    \end{equation*}

    We want to show that $g_{\bigcup X_n\cap \alpha_n}\in L_{f(\gamma)}$. Since $\mathrm{cf}\, \gamma = \omega$ and $\pi(D_n)$ are open dense we impose another restriction on $t_n$ s.t. they satisfy $\sup \alpha_n = \gamma$. Let $\beta < \gamma$ consider $\left(\bigcup X_n\cap \alpha_n\right)\cap \beta = \bigcup X_n \cup \beta $. By construction

    \begin{equation*}
        \forall n <\omega,\, X_n\cap\beta \in L_{f(\beta )}\wedge L_{f(\beta)}\prec L_{\omega_1}
    \end{equation*}

    Thus, $\left< X_n\cap \beta\: \big|\: n <\omega\right>\in L_{f(\beta)}$ therefore $\bigcup X_n\cap\beta\in L_f(\beta)$ and $\bigcup X_n \cap \alpha_n \in \mathcal{F}$.

    Since $\mathbb{T}$ is distributive it does not add any reals (lemma \ref{lem:distributive_no_reals}) hence also does not collapse $\omega_1$.

    We show $\mathbb{T}$ is $\aleph_2$-c.c., let $A$ be an anti-chain of cardinality $\aleph_2$. Consider the set of domains of the conditions in $A$. Since the domains $\alpha<\omega_1$ there exists $\alpha <\omega_1$ s.t. $A_1 = \left\{ f\, \big|\, f\in A,\, \mathrm{dom}\: f = \alpha \right\}$ and $|A_1| =\aleph_2$. The cardinality of the set of functions  $f:\alpha \rightarrow \mathcal{P}(\alpha)$ is ${(2^{\alpha})}^{\alpha} = 2^{\aleph_0\cdot\aleph_0} = 2^{\aleph_0} = \aleph_1$ since $L\models \textbf{GCH}$. Thus $\exists A_2\subset A_1$ s.t $\forall f,g\in A_2,\, g = f$, contradiction.
\end{proof}

\begin{definition}
    A Kurepa tree $T$ in a model $M$ is called \textbf{sealed} if $\forall\mathbb{P}\in M$ s.t.

    \begin{equation*}
        \mathbb{P}\Vdash \exists \text{ a new branch for } T
    \end{equation*}

    Then $\mathbb{P}$ collapses $\omega_1$ or $\omega_2$, i.e. one cannot add a branch without ruining the Kurepa property.
\end{definition}

\begin{theorem}[Shelah and Poor \cite{Poor}]
    \label{thm:poor}
    Assume that there exists $A\subset \omega_1$ s.t.
    \begin{equation*}
        \omega_1^{L[A]} = \omega_1^V \wedge \omega_2^{L[A]} = \omega_2^V
    \end{equation*}
    then there exists a Kurepa tree with $[T]\subset L[A]$.
\end{theorem}

\begin{corollary}
    The Kurepa tree constructed in Theorem \ref{thm:poor} is sealed.
\end{corollary}

\begin{proof}
    Let $\mathbb{P}$ be some forcing poset and $G\subset \mathbb{P}$ a generic filter. Assume that

    \begin{equation*}
        \mathbb{P}\Vdash \omega_1^V = \omega_1^{V[G]}\wedge \mathbb{P}\Vdash \omega_2^V = \omega_2^{V[G]}
    \end{equation*}

    But then the construction of $L[A]$ is absolute thus $L[A]^V = L[A]^{V[G]}$. Since all the branches of $T$ are in $L[A]$ no branches are added.
\end{proof}

The definitions regarding properness are taken from \cite{Uri}.

\begin{definition}
    Let $\mathbb{P}$ be a forcing notion, $\lambda > 2^{\mathbb{P}}$ and $M\prec H(\lambda)$.

    A condition $p\in \mathbb{P}$ is called $M$-generic iff for every dense $D\subset \mathbb{P}$ s.t. $D\in M$ and for every $q\leq p$ there exists $d\parallel q$ s.t. $d\in D\cap M$.
\end{definition}

\begin{definition}
    A poset $\mathbb{P}$ is said to be proper for $M\prec H(\lambda)$ if $\forall p\in \mathbb{P}$, $\exists q\leq p$ s.t. $q$ is an $M$-generic condition.
\end{definition}

\begin{definition}
    For a set of models $\mathcal{M}$ we say $\mathbb{P}$ is proper for $\mathcal{M}$ if $\mathbb{P}$ is proper for all $M\in \mathcal{M}$.
\end{definition}

Here we bring definitions and results from \cite{Neeman} that we use in this work.

\begin{definition}
    \label{def:S_and_T}
    ${\cal S}$ and ${\cal T}$ are appropriate for cardinals $\kappa$,
    $\lambda$ and a transitive model $\left(K,\in\right)$ satisfying
    enough of \textbf{ZFC} and by enough we mean: closed under
    pairing, union, intersection, set difference, Cartesian product and
    transitive closure. Closed under range and restriction on functions
    and that for each $x\in K$ the closure of $x$ under intersections
    belongs to $K$, there is a bijection from an ordinal onto $x$ in
    $K$ and there is a sequence in $K$ consisting of members of $x$
    arranged in a non-decreasing Von-Neumann rank. If:
    \begin{enumerate}
        \item $\mathcal{T}$ is a collection of transitive $W\prec K$, and $\mathcal{S}$
              is a collection of $M\prec K$ with $\kappa\subset M$ and $\left|M\right|<\lambda$. $\forall N \in \mathcal{S}\cup\mathcal{T},\ N\in K,\ \left\{ \kappa,\lambda \right\}\in N$.

        \item $\forall M_{1},M_{2}\in{\cal S}$ and $M_{1}\in M_{2}$ then $M_{1}\subset M_{2}$.
        \item $W\in{\cal T}$ and $M\in{\cal S}$ and $W\in M$ then $M\cap W\in W$
              and $M\cap W\in{\cal S}$.
        \item Each $W\in{\cal T}$ is closed under sequences of length $<\kappa$.
    \end{enumerate}
\end{definition}

\begin{remark}
    Here, we concentrate on the definition \ref{def:S_and_T} in the case $\kappa =\omega$, $\lambda = \omega_1$ and $K = H(\omega_2)$. Thus, \cal{S} becomes a collection of countable elementary submodels, and $\left( 2,4 \right)$ follow from elementarity. From now on we present only the discussed case (though we encourage reading about the general approach in \cite{Neeman}).
\end{remark}

\begin{definition}
    For $\mathcal{T},\mathcal{S}$ appropriate for $\omega,\omega_1,K$ we define the two-type model sequence poset $\mathbb{M} =\mathbb{M}_{\omega,\mathcal{S},\mathcal{T},K}$. $p\in\mathbb{M}$ iff:
    \begin{enumerate}
        \item $p = \left< M_m\ \Big|\ m < n\right>$ is a finite sequence that belongs to $K$ and $\forall m,\ M_m\in\mathcal{S}\cup\mathcal{T}$.
        \item The sequence is $\in$-increasing, $M_{m - 1} \in M_{m}$ for $0 < m < n$
        \item The sequence is closed under intersections, $\forall k,m < n,\ M_k\cap M_m$ is in $p$.
    \end{enumerate}
\end{definition}

\begin{definition}
    For $p,q\in\mathbb{M}$, where $p =\left< M_\xi\ \Big|\ \xi <\gamma_p\right>$ and $q =\left< M_\xi\ \Big|\ \xi <\gamma_q\right>$ we define $p\leq_\mathbb{M}q$ iff $\left\{ M_\xi\ \Big|\ \xi <\gamma_p \right\} \supset \left\{ M_\xi\ \Big|\ \xi <\gamma_q \right\}$.
\end{definition}

\begin{definition}
    Let $p\in\mathbb{M}$ and let $Q\in p$ define the residue of $p$ in $Q$.
    \begin{equation*}
        res_Q\left( p \right) = \left\{ M\in p\ \Big|\ M\in Q \right\}
    \end{equation*}
\end{definition}

\begin{lemma}
    \label{lem:why_d_M_is_a_condition}
    Let $p\in\mathbb{M}$ and $Q\in p$ then $res_Q\left( p \right)\in \mathbb{M}$.
\end{lemma}

\begin{definition}
    Conditions $p,q\in\mathbb{M}$ are compatible if $\exists r \in \mathbb{M}$ s.t. $r\supset p\cup q$ and are directly compatible if $r$ is the closure of $p\cup q$ under intersections.
\end{definition}

\begin{lemma}
    \label{lem:q_leq_res}
    Let $p\in \mathbb{M}$, $Q\in p$ and let $q\in Q\cap\mathbb{M}$ s.t. $q\leq res_Q\left( p \right)$ then $p,q$ are directly compatible.
\end{lemma}

\begin{corollary}
    \label{cor:p_cup_M}
    Let $M\in\mathcal{S}\cup\mathcal{T}$ and let $p\in M\cap\mathbb{M}$ then $\exists q \leq p$ s.t. $M\in q$. Moreover, $q$ can be taken to be the closure of $p\cup \left\{ M \right\}$ under intersections.
\end{corollary}








