\section{The Generic Object}

Let $G\subset \mathbb{S}$ be a generic filter. Consider the function

\begin{equation*}
    f_G = \bigcup_{ \left< f,\vec{M}\right>\in G} f
\end{equation*}

\begin{claim}
    $range(f) =\omega$
\end{claim}

\begin{proof}
    Consider the set

    \begin{equation*}
        D_n = \left\{ p\, \big| n\in range(f_p) \right\},\quad n <\omega
    \end{equation*}

    We show it is dense. Let $q\in \mathbb{S}$ be some condition and let $\pi_1 : T\backslash T_0\rightarrow T_1$ be the projection on the first level of the tree. Since $\left| T_1 \right| = \aleph_0$ we have that $T_1 \backslash \pi_1 \left(\mathrm{dom}f_q\right)\neq\emptyset$. Choose $x\in T_1 \backslash \pi_1 \left(\mathrm{dom}f_q\right) $ then

    \begin{equation*}
        D_n \ni \left< f_q\cup \left< x,n \right>,  \vec{M}_q \right> = q' \leq q
    \end{equation*}

    Thus, $D_n$ is dense.
\end{proof}

\begin{claim}
    \label{claim:dom_f_G}
    $\mathrm{dom}f_G = T\cup \left[ T \right]$
\end{claim}

\begin{proof}
    Let $x\in T\cup \left[ T \right]$ and consider

    \begin{equation*}
        D_x = \left\{ p\, \big|\, x\in \mathrm{dom}f_p \right\}
    \end{equation*}

    We show it is dense. Let $q\in \mathbb{S}$ be some condition. We want to add $x$ to $f_q$ without changing $\vec{M}_q$ s.t. the new function is consistent with all the requirements of a condition in definition \ref{def:forcing_poset}. We separate the construction into two cases, if $\exists y\in \mathrm{dom}f_q,\, x\leq_T y$ and otherwise.

    Assume $\exists y\in \mathrm{dom}f_q,\, x\leq_T y$ then $\forall z\in \mathrm{dom}f_q,\, z\wedge x \in \mathrm{dom}f_q\cup \{x\}$ since if $z\geq_T x$ then $z\wedge x = x$ (by lemma \ref{lem:wedge_properties}) and if $x\nleq z$ then $x\wedge z = y\wedge z$ (lemma \ref{lem:wedge_properties}). We need to determine the value of $f$ on $x$ and, we need to make sure that our choice is robust enough to take into account the case where the value of $f(x)$ is determined via a branch in some $N\in \vec{M}_q$.

    Define

    \begin{equation*}
        f(x) = \min \left\{ f(y)\, \Big|\, x\leq y,\, y\in \mathrm{dom}\: f \right\}
    \end{equation*}

    Assume $x\in b\in N$ and $level(x)\geq N\cap \omega_1$. Then by condition \ref{condition:2}, $b\in \mathrm{dom}\: f$. Let $x\leq y$ then $x\leq (y\wedge b) $ hence $f(y\wedge b) = f(b)$ and $f(x) = f(b)$. Thus, this choice is consistent with condition \ref{condition:2}.

    Next we add all the \underline{new} branches required by condition (\ref{condition:2}), this requires that for $N \in \vec{M}_q$ and $b\in N$ s.t. $x\in b$ and $N\cap\omega_1 \leq level(x)$ we add $b$ to the domain of our new function and set its value $f(b) = f(x)$ (condition \ref{condition:2}). First notice that we add a finite number of branches, that is because each model in the model sequence (which is finite) can add at most one branch. To show that assume some model $N$ in the model sequence has two branches $b_1,b_2\in N$ s.t. $x\in b_1,b_2$ and $level(x) > N\cap \omega_1$, then $N\models b_1 = b_2$ and from elementary we get $b_1 = b_2$. We claim that the insertion of all this new branches is consistent. Let $x\leq y\in \mathrm{dom}f_q$ then $y\notin b$ for all new $b$ since then $b$ would already have been in the domain and, it would have set the value of $f(x)$ to $f(b)$.

    Next we add all the highest-common-predecessors of the new branches and set their values to $f(x)$. We claim that the insertion of all the highest common ancestors is  consistent. Let $x\leq y\in \mathrm{dom}f_q$, since $y$ is not on any one of the new branches if $y\parallel \bigwedge_i b_i$ then $\bigwedge_i b_i \leq y$, where $b_i$ are new branches added on the previous step. Since $f\left(\bigwedge_i b_i\right) = f(x) \leq f(y)$ we are ok unless $\bigwedge_i b_i$ is on a branch $b'$ determining $f(y)$.

    Let's assume that $f(x) < f(b')$ hence $\exists z \in\mathrm{dom}f_q$ s.t. $f(z) < f(b')$. Consider $x\leq (z\wedge b')\in \mathrm{dom}f_q$ since $f(z\wedge b') \leq f(z) < f(b') = f(y)$ we have that $z\wedge b' < \bigwedge_i b_i $ else $z\wedge b'$ would have determined $b'$. Hence, $z\wedge b' \in b_i$ for all $i$ i.e. $b_i \in \mathrm{dom}f_q$ contradiction.

    If $y \perp \bigwedge_i b_i$ the argument is the same. We now need to add the highest-common-predecessors of the new branches with the elements which were previously in the domain.

    Notice that after this steps we have new elements, all are higher than $x$ therefore can add branches via condition (\ref{condition:2}). Repeating the above process for the new added elements will give us new elements to add which are even higher.


    \begin{claim}
        This process will terminate after a finite number of steps.
    \end{claim}

    \begin{proof}
        A model $N$ adds branch only if there is some element above $x$ in the domain above the $N\cap\omega_1$-level of the tree. Assume $x$ added a branch from $N$ then all new elements above $x$ can not add more branches from $N$. Since we only add elements above new elements we eventually come to a stop.
    \end{proof}

    For the second case if $\left\{ y\, \Big|\, x\leq y \right\} = \emptyset $ we need to add $\left\{ x\wedge y\, \Big|\, y\in \mathrm{dom}f_q \right\}$. We claim that we add at most one element. Let $x\wedge y_{\max }$ be the highest element added. Then by lemma \ref{lem:wedge_properties}, $\forall z\in \mathrm{dom}f_q,\, z\wedge x = z\wedge y_{\max }$. Then we take $f(x\wedge y_{\max }) = f(x) = \min \left\{ z\, \Big|\, x\wedge y_{\max } \leq z \right\}$. Here we need to add the branches of $x$ and of $x\wedge y$. Since there are no elements above $x$ the addition of elements above $x$ can interact with the rest of the function only below $x$ and since $x$ is proved to be consistent all the elements added above it are also consistent. The addition of the branches of $x\wedge y$ is consistent by the same argument as for the first case.

    Let $f$ be the function after applying the above construction. Then $q' = \left< f, \vec{M}_q\right>$ is a condition in $D_x$ and $q'\leq q$. Thus, $D_x$ is dense.
\end{proof}

\begin{theorem}
    $\forall \mathbb{Q} \in H(\omega_3)$

    \begin{equation*}
        \mathbb{Q}\Vdash \dot{b}\subset \check{T}\mathrm{\, is\, a\, new\, branch}
    \end{equation*}

    then

    \begin{equation*}
        \mathbb{S}\times \mathbb{Q}\Vdash \left|\check{\omega}_1^V\right| = \aleph_0
    \end{equation*}
\end{theorem}

\begin{proof}
    Let $\mathbb{Q}$ be a forcing s.t.
    \begin{equation*}
        \mathbb{Q}\Vdash \dot{b}\subset \check{T}\mathrm{\, is\, a\, new\, branch}
    \end{equation*}

    Consider the set
    \begin{equation*}
        D_n = \left\{ (p,q)\in \mathbb{S}\times\mathbb{Q}\, \Big|\, q\Vdash \check{x}\in \dot{b}\wedge p\Vdash \dot{f}_G(\check{x}) >\check{n} \right\}
    \end{equation*}
    We show it is dense in $\mathbb{S}\times \mathbb{Q}$. Let $(p_1,q_1)\in \mathbb{S}\times \mathbb{Q}$ be a condition. Since

    \begin{equation*}
        q_1 \Vdash \dot{b}\subset \check{T} \mathrm{\, is\, a\, new\, branch}
    \end{equation*}
    Let $q_{2,\alpha}\leq q_1$ be the condition which determines the element in the $\alpha$-level i.e.

    \begin{equation*}
        q_{2,\alpha}\Vdash \check{x}_\alpha\in \dot{b}\cap \check{T}_\alpha
    \end{equation*}

    Let $\beta = \sup \left\{ level(x)\, \Big|\, x\in \mathrm{dom}f_{q_1}\backslash \left[ T \right] \right\}$, since the domain is finite $\beta < \omega_1$. Consider $q_{2,\beta + 1}$, let

    \begin{equation*}
        B_1 = \left\{ b\, |\, b\in \mathrm{dom}f_{q_1}\cap\left[ T \right]\right\}
    \end{equation*}

    And

    \begin{equation*}
        B_2 = \left\{ b\, |\, \exists N\in \vec{M}_{q_1}\cap\mathcal{S}, b\in N \right\}
    \end{equation*}

    And define $B = \bigcup\left( B_1\cup B_2 \right)$. Since $\vec{M}_{q_1}\cap\mathcal{S}$ is finite and each model in it holds at most $\aleph_0$ branches, $B_2\cup B_1$ is countable. Hence, $\exists \gamma <\omega_1$ and
    a condition $q_{2,\gamma}\leq q_{2,\beta+ 1}$ s.t.

    \begin{equation*}
        q_{2, \gamma} \Vdash \check{B}\cap \cdot{b}\cap\check{T}_\gamma = \emptyset
    \end{equation*}

    We take $q_3 \leq q_{2, \gamma}$ s.t.
    \begin{equation*}
        q_3\Vdash \check{x} \in \dot{b}\cap\check{T}_\gamma
    \end{equation*}

    Define $f_{p_2}' = f_{p_1} \cup \left< x,\max \{n + 1, \sup range(f_{p_1})\}\right>$, to make  $p_2 = \left< f_{p_2}', \vec{M}_{p_1} \right>$ a viable condition we need to add $x\wedge y_{\max }$ and use the same process as in the proof of claim \ref{claim:dom_f_G} (which was proven to be consistent). Denote the added function by $f_{add}$. And let  $\left< f_{p_2}'\cup f_{add}, \vec{M}_{p_1} \right>$. We claim this is a condition, since $f_{add}$ is consistent and the  new $x$ is not on any of the branches in the models in the model-sequence and not on any branches in the domain of $f_{p_1}$ there is no restriction on its value except for monotonicity, since its value was chosen to be higher than all previous results of the function $p_2 = \left< f_{p_2}'\cup f_{add}, \vec{M}_{p_1} \right>$ defines a condition. Since $(p_2,q_3)\leq (p_1,q_1)$ and $(p_2,q_3)\in D_n$, $D_n$ is dense.

    Let $G\subset \mathbb{S}\times \mathbb{Q}$ be a generic filter and let $f_G$ be the generic function and $b_G$ the generic branch. Since $D_n\cap G\neq \emptyset$ for all $n\in \mathbb{N}$ we have

    \begin{equation*}
        \sup range(f_G\restriction b_G) = \omega
    \end{equation*}

    $f_G$ is a monotone unbounded function between $b_G$ (which is isomorphic to $\omega_1^V$) and $\omega$. Thus, $\left|\omega_1^V\right| = \aleph_0$.

\end{proof}